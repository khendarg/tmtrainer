#!/usr/bin/env python2

import pymol
import os
import sys
import argparse
import Bio.PDB
try: CODE = Bio.PDB.protein_letters_3to1
except AttributeError: CODE = Bio.PDB.to_one_letter_code

def get_indices(pdbc, tmdatadir):
	indices = {}
	with open('{}/opm/ASSIGNMENTS.TSV'.format(tmdatadir)) as f:
		for l in f:
			if l.startswith(pdbc[:4].lower()):
				if (len(pdbc) > 4) and (l[5] != pdbc[-1]): continue
				indices[l[5]] = [span for span in l.strip().split('\t')[1].split(',')]
	return indices

def check_lengths():
	lengthenme = []
	shortenme = []
	for sele in pymol.cmd.get_names('selections'):
		if not sele.startswith('c'): continue
		elif '_tms' not in sele: continue
		else:
			tmpspace = {'resilist':set(), 'int':int}
			tmcoords = pymol.cmd.iterate(sele, 'resilist.add(int(resi))', space=tmpspace)
			nres = max(tmpspace['resilist']) - min(tmpspace['resilist']) + 1

			if 17 <= nres <= 25: pymol.cmd.color('green', sele)
			elif nres < 17: 
				lengthenme.append([sele, 17-nres])
				pymol.cmd.color('red', sele)
			else:
				shortenme.append([sele, nres-25])
				pymol.cmd.color('red', sele)

	if shortenme:
		print('THE FOLLOWING NEED TO BE SHORTENED (TO 25):')
		for name, n in shortenme: print(' - {} by {}aa'.format(name, n))
	if lengthenme:
		print('THE FOLLOWING NEED TO BE LENGTHENED (TO 17):')
		for name, n in lengthenme: print(' - {} by {}aa'.format(name, n))
	if not (shortenme or lengthenme): print('ALL TMS LENGTHS LOOK OK. PLEASE COMMIT THESE RESULTS! (> commit)')

def get_states(chain, indices, startstate):
	tmpspace = {'residues':{}, 'int':int}
	tmpcoords = pymol.cmd.iterate('c. ' + chain + ' and n. CA', 'residues[int(resi)] = {"resn":resn, "resi":resi}', space=tmpspace)

	spans = []
	for span in indices: spans.append([int(i) for i in span.split('-')])

	seq, states = [], []
	for i in range(1, max(tmpspace['residues'])+1):
		if i in tmpspace['residues']:
			if tmpspace['residues'][i]['resn'] in CODE: seq.append(CODE[tmpspace['residues'][i]['resn']])
			else: continue
		else: seq.append('X')

		if i == 1: states.append(startstate[0])
		else:
			found = False
			for span in spans:
				if span[0] <= i <= span[1]: 
					states.append('H')
					found = True
					break
			if not found: states.append('_')

	inverse = dict(zip('IiOo', 'OoIi'))
	state = states[0]
	flipnext = False
	for i in range(len(states)):
		if states[i] == 'H':
			if flipnext: continue
			else: flipnext = True
		elif states[i] == '_':
			if flipnext: 
				state = inverse[state]
				flipnext = False
			states[i] = state

	for tm in spans:
		for i in range(tm[0]-1-1, max(-1, tm[0]-1-1-15), -1):
			if states[i] == 'H': break
			states[i] = states[i].lower()
		for i in range(tm[1], min(len(states), tm[1]+15)):
			if states[i] == 'H': break
			states[i] = states[i].lower()
			
	return ''.join(seq), ''.join(states)

def main(pdbclist, tmdatadir, outfile='newasssignments.tsv', clobber=False):
	sys.argv = sys.argv[:1] + ['-q']
	pymol.finish_launching()
	pymol.cmd.extend('chklen', check_lengths)
	index = [0]
	#TODO: wrap up most of this stuff in a class to kill off these mutability hacks
	def _do_stuff(index=index):
		i = index[-1]
		pdbc = pdbclist[i]
		startstate = ['NONE']
		def commit_results(fn=outfile, startstate=startstate):
			with open(fn, 'a') as f:

				spans = {}
				for sele in pymol.cmd.get_names('selections'):
					if not sele.startswith('c'): continue
					elif '_tms' not in sele: continue
					else:
						if sele[1] not in spans: spans[sele[1]] = []
						tmpspace = {'resilist':set(), 'int':int}
						tmcoords = pymol.cmd.iterate(sele, 'resilist.add(int(resi))', space=tmpspace)
						spans[sele[1]].append('{}-{}'.format(min(tmpspace['resilist']), max(tmpspace['resilist'])))
				for chain in sorted(spans):
					s = pdbc[:4].lower()
					s += '_{}'.format(chain)
					s += '\t'
					s += startstate[-1]
					s += '\t'
					s += ','.join(spans[chain])

					seq, states = get_states(chain, spans[chain], startstate[-1])
					s += '\t{}'.format(seq)
					s += '\t{}'.format(states)

					f.write('{}\n'.format(s))
				print('WROTE RESULTS TO {}'.format(fn))

		pymol.cmd.delete('*')
		pymol.cmd.remove('*')
		pymol.cmd.load('{}/pdb/{}.pdb'.format(tmdatadir, pdbc[:4].lower()))
		if len(pdbc) > 4: pymol.cmd.remove('not c. {}'.format(pdbc[-1]))
		indices = get_indices(pdbc, tmdatadir)

		for chain in indices:
			tmsel = []
			for j, span in enumerate(indices[chain]):
				pymol.cmd.select('c{}_tms{}'.format(chain, j+1), 'c. {} and i. {}'.format(chain, span))
				tmsel.append('c{}_tms{}'.format(chain, j+1))

			pymol.cmd.deselect()

			tm1coords = pymol.cmd.get_coords(tmsel[0] + ' and n. N+CA+C')
			if (tm1coords[-1] - tm1coords[0])[2] < 0: startstate.append('OUT')
			else: startstate.append('IN')


			#for i in range(100): pymol.cmd.rotate('x', 360)
			pymol.cmd.color('white', 'c. {}'.format(chain))


			#pymol.cmd.center(' '.join(tmsel))
		pymol.cmd.set('seq_view')
		pymol.cmd.hide('li')
		pymol.cmd.show('car')
		pymol.cmd.rotate('x', -90)
		pymol.cmd.extend('commit', commit_results)
		check_lengths()
	def nextprot(index=index):
		index.append(index[-1]+1)
		_do_stuff(index)
	def prevprot():
		index.append(index[-1]-1)
		_do_stuff(index)
	_do_stuff(index)
	pymol.cmd.extend('nextprot', nextprot)
	pymol.cmd.extend('prevprot', prevprot)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	if 'TMDATA' in os.environ: parser.add_argument('--tmdatadir', default=os.environ['TMDATA'])
	else: parser.add_argument('--tmdatadir', required)
	parser.add_argument('pdbc', nargs='+', help='PDB to inspect')
	parser.add_argument('-o', default='newassignments.tsv', help='Where to write the results')
	parser.add_argument('-f', action='store_true', help='Overwrite the results file instead of appending to it')

	args = parser.parse_args()

	main(pdbclist=args.pdbc, tmdatadir=args.tmdatadir, outfile=args.o, clobber=args.f)

