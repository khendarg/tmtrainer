Contents:

 - opmweaver.py - Pymol script for adjusting OPM TMSs to better fit HMMTOP's TMS length limits (17-25aa)
 - pseudocounter.py - Convert the output of opmweaver.py into something resembling HMMTOP's PSV
 - tmtrainer.py - TMWeaver fork for adjusting HMMTOP's TMSs and sending the resulting data to a central repository
 - tmtrainer\_save.py  - TMTrainer endpoint for saving TMS predictions
