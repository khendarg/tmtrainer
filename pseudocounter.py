#!/usr/bin/env python3

import argparse
import subprocess
import re
import sys
import os

class StateError(Exception): pass
class BlastError(Exception): pass

def warn(*things):
	print('\033[33m[WARNING]\033[0m', *things, file=sys.stderr)

def check_states(states):
	m = re.findall('H{26,}', states)
	if m: raise StateError('Overly long (>25aa) helix detected ({})'.format(m))
	if 'H' in states:
		for m in re.findall('H+', states):
		#m = re.findall('H{1,16}', states)
			if len(m) < 17: raise StateError('Overly short (<17aa) helix detected ({})'.format(m))
	m = re.findall('i{16,}I*i{15,}|i{15,}I*i{16,}', states)
	if m: raise StateError('Overly long (>30aa) itail detected ({})'.format(m))
	m = re.findall('o{16,}O*o{15,}|o{15,}O*o{16,}', states)
	if m: raise StateError('Overly long (>30aa) itail detected ({})'.format(m))
	m = re.findall('O[IiH]|I[OoH]|iO|oI|H[OI]', states)
	if m: raise StateError('(Actually) forbidden transition detected ({})'.format(m))

def expand_states(states):
	longstates = []
	for i in range(len(states)):
		if states[i] == 'O': longstates.append('oloop.1')
		elif states[i] == 'I': longstates.append('iloop.1')
		elif states[i] == 'i': longstates.append('itail.')
		elif states[i] == 'o': longstates.append('otail.')
		elif states[i] == 'H': 
			if states[i-1] in 'Oo': longstates.append('helixo.')
			elif states[i-1] in 'Ii': longstates.append('helixi.')
			else: longstates.append(longstates[i-1])

	skip = 0
	firsttail = True
	finaltail = False
	for i in range(len(longstates)):
		if skip > 0:
			skip -= 1
			continue
		if longstates[i].startswith('helix'):
			l = 0
			for j in range(len(longstates[i:])): 
				if not longstates[i+j].startswith('helix'): break
				else: longstates[i+j] += str(j+1)
				l += 1
			skip += l - 1
		elif 'tail' in longstates[i]:
			l = 0
			ltot = 0
			finaltail = True
			for j in range(len(longstates[i:])):
				if 'helix' in longstates[i+j]: 
					finaltail = False
					break

				ltot += 1
				if 'tail' in longstates[i+j]: l += 1
			if firsttail:
				if l > 15: raise StateError('Too many tail residues in N-terminal loop')
				n = 30 - l + 1
				for j in range(ltot):
					if 'tail' in longstates[i+j]:
						longstates[i+j] += str(n)
						n += 1
				firsttail = False
			elif finaltail:
				if l > 15: raise StateError('Too many tail residues in C-terminal loop')
				n = 1
				for j in range(ltot):
					if 'tail' in longstates[i+j]:
						longstates[i+j] += str(n)
						n += 1
				#finaltail = True #for all the good that does
			elif l >= 30:
				if l > 30: raise StateError('How did this happen????')
				if ltot == 30:
					for j in range(ltot):
						if 'tail' in longstates[i+j]: longstates[i+j] += str(j+1)
				elif ltot > 30:
					n = 1
					for j in range(ltot):
						if 'tail' in longstates[i+j]: 
							longstates[i+j] += str(n)
							n += 1
				else: raise StateError('0 == 1')
			elif l < 30:
				pre = [1]
				post = [30]
				for j in range(l-2):
					if j % 2: post.insert(0, post[0]-1)
					else: pre.append(pre[-1]+1)
				catted = pre + post
				for j in range(ltot):
					if 'tail' in longstates[i+j]:
						longstates[i+j] += str(catted.pop(0))
			#pre = [1]
			#post = [30]
			#print(l)
			#for j in range(l-2):
			#	if j % 2: post.insert(0, 30-j//2 - 1)
			#	else: pre.append(j//2 + 2)
			#print(l, ltot, pre, post)
			#catlist = pre + post
			#for j in range(len(longstates[i:i+ltot])):
			#	if 'tail' in longstates[i+j]:
			#		longstates[i+j] += str(catlist.pop(0)+1)
			skip += ltot - 1
	return longstates

def count_transitions(longstates, transitions=None):
	transitions = {} if transitions is None else transitions
	for i in range(1, len(longstates)):
		if longstates[i-1] not in transitions:
			transitions[longstates[i-1]] = {}
		if longstates[i] not in transitions[longstates[i-1]]:
			transitions[longstates[i-1]][longstates[i]] = 1
		else:
			transitions[longstates[i-1]][longstates[i]] += 1
	return transitions

def simplify_fasta(fastastr):
	header = fastastr[:fastastr.find('\n')]
	seq = fastastr[fastastr.find('\n')+1:].replace('\n', '')
	return [header, seq]

def blast(seq, db, label, max_target_seqs=5):
	cmd = ['blastp', '-db', db, '-outfmt', '7', '-max_target_seqs', str(max_target_seqs)]
	p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
	blastout, err = p.communicate(input=seq.encode('utf-8'))
	blastout = blastout.decode('utf-8')


	if not blastout.strip(): raise BlastError('Could not find a close full sequence for {}'.format(label))
	for l in blastout.split('\n'):
		if not l.strip(): continue
		elif l.startswith('#'): continue
		sl = l.split('\t')

		qstart = int(sl[6])
		qend = int(sl[7])
		sstart = int(sl[8])
		send = int(sl[9])

		cmd = ['blastdbcmd', '-db', db, '-entry', sl[1], '-target_only']
		truefasta = subprocess.check_output(cmd).decode('utf-8')
		simpfasta = simplify_fasta(truefasta)

		if qstart > sstart: 
			extrahead = 'X' * (qstart - sstart)
			head = 0
		elif sstart > qstart:
			extrahead = ''
			head = sstart - qstart
		else: #sstart == qstart
			extrahead = ''
			head = 0

		newseq = extrahead + simpfasta[1][head:]

		if len(newseq) > len(seq): newseq = newseq[:len(seq)]
		elif len(newseq) < len(seq): newseq += 'X' * (len(seq) - len(newseq))

		return newseq
	return seq

def count_emissions(sequence, states, emissions=None):
	emissions = {} if emissions is None else emissions

	for state, resn in zip(states, sequence):
		if resn not in 'ACDEFGHIKLMNPQRSTVWY': continue
		if state not in emissions: emissions[state] = {}
		if resn not in emissions[state]: emissions[state][resn] = 0
		emissions[state][resn] += 1

	return emissions

def dump_starts(starts):
	O = starts.get('O', 0)
	I = starts.get('I', 0)
	s = ''

	for i in range(112):
		if i == 0: s += str(O).rjust(8)
		elif i == 31: s += str(I).rjust(8)
		else: s += str(0).rjust(8)
	return s

def dump_emissions(emissions):
	s = ''
	for state in 'OoIiH':
		rescounts = emissions.get(state, {})
		for resn in 'ACDEFGHIKLMNPQRSTVWY':
			count = rescounts.get(resn, 0)
			s += str(count).rjust(8)
		s += '\n'
	return s.rstrip()

class ODict(object):
	def __init__(self, keys, values):
		self.keys = keys
		self.values = values

	def __getitem__(self, index):
		if index not in self.keys: raise KeyError
		return self.values[self.keys.index(index)]

	def __setitem__(self, index, newvalue):
		if index not in self.keys: raise KeyError
		self.values[self.keys.index(index)] = newvalue

	def get(self, index, default=None):
		if index not in self.keys: return default
		return self[index]

	def __iter__(self): return iter(self.keys)

def unpack_transitions(tstr):
	pieces = re.split(', *', tstr)
	transitions = []
	for piece in pieces[1:]:
		if piece == 'theone': continue
		elif '-' in piece:
			span = [int(x) for x in piece.split('.')[1].split('-')]
			for i in range(span[0], span[1]+1):
				transitions.append(piece.split('.')[0] + '.' + str(i))
		else: transitions.append(piece)
	return transitions

def load_arch(fn):
	states = []
	transitions = []
	with open(fn) as f:
		for l in f:
			if l.startswith('state {'): states.append(l.strip().split()[-1])
			elif l.strip().startswith('transition = '): 
				transitions.append(unpack_transitions(l[l.find('=')+2:-1]))
	return states, transitions

def dump_transitions(transitions):
	states, trtables = load_arch(os.environ['HMMTOP_ARCH'])
	trdict = {}
	for state, nextstates in zip(states, trtables):
		if len(nextstates) == 1: 
			if 'theone' not in trdict: trdict['theone'] = [0]
		else: trdict[state] = [0] * len(nextstates)
	for state, nextstates in zip(states, trtables):
		counts = transitions.get(state, {})
		for i, nstate in enumerate(nextstates):
			if len(nextstates) == 1:
				trdict['theone'][0] += counts.get(nstate, 0)
				break
			else:
				trdict[state][i] += counts.get(nstate, 0)

	#print(transitions)
	#fold helix states together
	for state, nextstates in zip(states, trtables):
		if state.startswith('helixo'):
			#rewrite for legibility and performance
			if len(nextstates) > 1:
				otherstate = state[:5] + 'i' + state[6:]
				if state not in trdict: trdict[state] = [0 for x in nextstates]
				if otherstate not in trdict: trdict[otherstate] = [0 for x in nextstates]
				for i in range(len(nextstates)): trdict[otherstate][i] += trdict[state][i]
				trdict.pop(state)

	s = ''
	for state in states:
		if state not in trdict: continue
		elif False: pass #helix
		else:
			for count in trdict[state]: s += str(count).rjust(8)
		s += '\n'
		if state == 'otail.15': s += str(trdict['theone'][0]).rjust(8) + '\n'
	return s.rstrip()

def dump_psv(starts, emissions, transitions):
	s = ''
	s += dump_starts(starts) + '\n'
	s += dump_emissions(emissions) + '\n'
	s += dump_transitions(transitions) + '\n'
	return s

def main(fn, db='uniprot_sprot', skip_errors=False, skip_blast=False):
	starts = {}
	emissions = {}
	transitions = {}
	with open(fn) as f:
		for l in f:
			if not l.strip(): continue
			elif l.startswith('#'): continue
			pdbc, startstate, spanstr, seq, states = l.replace('\n', '').split('\t')
			if skip_errors:
				try: check_states(states)
				except StateError as e: 
					warn(e)
					continue
			else: check_states(states)
			if skip_blast: truesequence = seq
			else:
				if skip_errors:
					try: truesequence = blast(seq, db, pdbc)
					except BlastError as e:
						warn(e)
						truesequence = seq
				else: truesequence = blast(seq, db, pdbc)

			if states[0] not in starts: starts[states[0]] = 0
			starts[states[0]] += 1

			count_emissions(truesequence, states, emissions)
			longstates = expand_states(states)
			count_transitions(longstates, transitions)
	#print(transitions)
	#print(emissions)
	return dump_psv(starts=starts, emissions=emissions, transitions=transitions)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	#TODO: Load $HMMTOP_ARCH instead of hardcoding stuff here
	parser.add_argument('--skip-blast', action='store_true', help='Skip BLASTing to speed up debugging')
	parser.add_argument('--skip-errors', action='store_true', help='Skip state sequences with errors (instead of crashing)')
	parser.add_argument('--db', default='uniprot_sprot', help='BLAST database containing full sequences for all PDBs. Do not use pdbaa. (default: uniprot_sprot')
	parser.add_argument('infile')
	parser.add_argument('outfile')
	args = parser.parse_args()

	psv = main(args.infile, db=args.db, skip_errors=args.skip_errors, skip_blast=args.skip_blast)
	with open(args.outfile, 'w') as f: f.write(psv)
